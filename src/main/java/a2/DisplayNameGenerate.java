package a2;

public interface DisplayNameGenerate {
    public String generateFrom(String firstName, String secondName);
}
