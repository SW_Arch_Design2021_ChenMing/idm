package a2;

public class addTauAddress implements EmailAddressGenerate {
    @Override
    public String generateFrom(String firstName, String secondName){
        String result;
        result = firstName + '.' + secondName + " @tau.fi";
        return result;
    }
}
