package a2;
import java.util.Scanner;
/**
 * Hello world!
 *
 */

public class App 
{
    
    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        int choice = 0;
        while(choice != 1 && choice != 2 && choice != 3){
            System.out.println("Enter a institution");
            System.out.println("Input 1 for TAMK, 2 for TAU, 3 for Normaalikoulu.");
            choice = myInput.nextInt();
        }

        switch(choice){
            case 1: 
                User myUser = new TamkUser();
                myUser.input();
                myUser.dataGenerate();
                myUser.addressGenerate();
                myUser.nameGenerate();
                myUser.dataPrint();
            break;
            case 2: 
                User myUser2 = new TauUser();
                myUser2.input();
                myUser2.dataGenerate();
                myUser2.addressGenerate();
                myUser2.nameGenerate();
                myUser2.dataPrint();
            break;
            case 3: 
                User myUser3 = new NorssiUser();
                myUser3.input();
                myUser3.dataGenerate();
                myUser3.addressGenerate();
                myUser3.nameGenerate();
                myUser3.dataPrint();
            break;

            default: break;
        }


    }




}
