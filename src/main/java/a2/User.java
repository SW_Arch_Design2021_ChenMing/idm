package a2;
import java.util.Scanner;
import java.util.Random;

public class User {

public
    String firstName;
    String secondName;
    String userType;
    String institution;
    int userID;
    String emailAddress;
    String displayName;

    EmailAddressGenerate emailAddressGenerate;
    public void addressGenerate(){
        emailAddress = emailAddressGenerate.generateFrom(this.firstName, this.secondName);
    }

    DisplayNameGenerate displayNameGenerate;
    public void nameGenerate(){
        displayName = displayNameGenerate.generateFrom(this.firstName, this.secondName);
    }

    public void input(){
        while(this.firstName == null){
            Scanner myInput = new Scanner(System.in);
            System.out.println("Enter a first name");
            this.firstName = myInput.nextLine();
        }

        while(this.secondName == null){
            Scanner myInput = new Scanner(System.in);
            System.out.println("Enter a second name");
            this.secondName = myInput.nextLine();
        }

        while(this.userType == null){
            Scanner myInput = new Scanner(System.in);
            System.out.println("Enter a user type");
            this.userType = myInput.nextLine();
        }

    }

    public void dataGenerate(){
        //generate a random number as a user ID
        Random rand = new Random();
        this.userID = rand.nextInt(100);
    /*
        //generate a email address using first and second name
        this.emailAddress = this.firstName + '.' + this.secondName + "@gmail.com";

        //generate a display name using first and second name
        this.displayName = this.firstName + ' ' + this.secondName + " (" + this.institution + ')';
    */
    }

    public void dataPrint(){
        System.out.println("This user's ID information is below...");
        System.out.println("User's first name is: " + this.firstName);
        System.out.println("User's second name is: " + this.secondName);
        System.out.println("User's user type is: " + this.userType);
        System.out.println("User's institution is: " + this.institution);
        System.out.println("User's user ID is: " + this.userID);
        System.out.println("User's email address is: " + this.emailAddress);
        System.out.println("User's display name is: " + this.displayName);
    }
}
